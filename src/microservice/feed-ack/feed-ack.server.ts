import { FeedAckModule } from './feed-ack.module';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { Config } from '../../core/config/Config';

async function serverBootstrap() {
  const app = await NestFactory.createMicroservice(FeedAckModule, {
    transport: Transport.RMQ,
    options: {
      urls: [
        Config.getRabbitMQConnectionUrl(),
      ],
      queue: Config.getFeedAckQueueName(),
      queueOptions: {
        durable: true,
      },
    },
  });
  await app.listen(() => console.log('Feed-ack is waiting for messages.'));
}

(() => serverBootstrap())();
