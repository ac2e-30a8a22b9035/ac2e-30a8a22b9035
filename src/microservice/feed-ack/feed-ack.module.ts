import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Config } from '../../core/config/Config';
import { FeedAckListener } from './feed-ack.listener';
import { FeedAckEventProcessorModule } from '../../core/module/feed-ack-event-processor/feed-ack-event-processor.module';

@Module({
  imports: [
    MongooseModule.forRoot(Config.getMongoDBConnectionUrl(), {}),
    FeedAckEventProcessorModule,
  ],
  controllers: [
    FeedAckListener,
  ],
  providers: [],
})
export class FeedAckModule {
}
