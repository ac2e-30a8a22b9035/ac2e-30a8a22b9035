import { EventPattern } from '@nestjs/microservices';
import { Controller } from '@nestjs/common';
import { FeedAckAckEvent } from '../../core/module/feed-ack-event-producer/feed-ack-ack.event';
import { FeedAckEventProcessorService } from '../../core/module/feed-ack-event-processor/feed-ack-event-processor.service';
import { FeedAckEvent } from '../../core/module/feed-ack-event-producer/feed-ack.event';

@Controller()
export class FeedAckListener {

  constructor(private readonly feedAckEventProcessorService: FeedAckEventProcessorService) {
  }

  @EventPattern(FeedAckEvent.ACK)
  async ack(event: FeedAckAckEvent) {
    const { userId, postIds } = event;
    await this.feedAckEventProcessorService.onPostAck(userId, postIds);
  }

}
