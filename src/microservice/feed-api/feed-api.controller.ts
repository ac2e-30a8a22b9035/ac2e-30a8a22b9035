import { Controller, Get, Put, Query } from '@nestjs/common';
import { UserContext } from '../../core/decorator/usercontext.decorator';
import { FeedService } from '../../core/module/feed/feed.service';
import { Post } from '../../core/module/post/post.interface';
import { PostRepository } from '../../core/module/post/post.repository';

@Controller()
export class FeedApiController {
  constructor(private readonly feedService: FeedService,
              private readonly postRepository: PostRepository) {
  }

  @Put('/post')
  public async initializePosts() {
    await this.postRepository.initializePosts(100000);
  }

  @Get('/feed')
  public async getFeed(@UserContext() userId: string,
                       @Query('id') id: string): Promise<Post[]> {
    return this.feedService.next(id);
  }
}
