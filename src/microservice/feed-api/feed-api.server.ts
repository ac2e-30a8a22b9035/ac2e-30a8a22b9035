import { FeedApiModule } from './feed-api.module';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function serverBootstrap() {
  const app = await NestFactory.create(FeedApiModule);
  const options = new DocumentBuilder().build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(3000);
}

(() => serverBootstrap())();
