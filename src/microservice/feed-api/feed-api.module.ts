import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FeedApiController } from './feed-api.controller';
import { Config } from '../../core/config/Config';
import { PostModule } from '../../core/module/post/post.module';
import { FeedModule } from '../../core/module/feed/feed.module';
import { FeedFanoutEventProducerModule } from '../../core/module/feed-fanout-event-producer/feed-fanout-event-producer.module';
import { FeedAckEventProducerModule } from '../../core/module/feed-ack-event-producer/feed-ack-event-producer.module';

@Module({
  imports: [
    MongooseModule.forRoot(Config.getMongoDBConnectionUrl(), {
    }),
    PostModule,
    FeedModule,
    FeedFanoutEventProducerModule,
    FeedAckEventProducerModule,
  ],
  controllers: [
    FeedApiController,
  ],
  providers: [],
})
export class FeedApiModule {
}
