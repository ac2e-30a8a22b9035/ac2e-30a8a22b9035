import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Config } from '../../core/config/Config';
import { FeedFanoutListener } from './feed-fanout.listener';
import { FeedFanoutEventProcessorModule } from '../../core/module/feed-fanout-event-processor/feed-fanout-event-processor.module';

@Module({
  imports: [
    MongooseModule.forRoot(Config.getMongoDBConnectionUrl(), {}),
    FeedFanoutEventProcessorModule,
  ],
  controllers: [
    FeedFanoutListener,
  ],
  providers: [],
})
export class FeedFanoutModule {
}
