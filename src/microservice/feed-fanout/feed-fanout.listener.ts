import { EventPattern } from '@nestjs/microservices';
import { Controller } from '@nestjs/common';
import { FeedFanoutEventProcessorService } from '../../core/module/feed-fanout-event-processor/feed-fanout-event-processor.service';
import { FeedFanoutPushEvent } from '../../core/module/feed-fanout-event-producer/feed-fanout-push.event';
import { FeedFanoutStartEvent } from '../../core/module/feed-fanout-event-producer/feed-fanout-start.event';
import { FeedFanoutEvent } from '../../core/module/feed-fanout-event-producer/feed-fanout.event';

@Controller()
export class FeedFanoutListener {

  constructor(private readonly feedFanoutEventProcessorService: FeedFanoutEventProcessorService) {
  }

  @EventPattern(FeedFanoutEvent.START)
  async onFanoutStartEvent(event: FeedFanoutStartEvent) {
    const { userId } = event;
    await this.feedFanoutEventProcessorService.onFanoutStart(userId);
  }

  @EventPattern(FeedFanoutEvent.PUSH)
  async onFanoutPushEvent(event: FeedFanoutPushEvent) {
    const { userId, optLock, rangeStart, rangeEnd } = event;
    await this.feedFanoutEventProcessorService.onFanoutPush(userId, optLock, rangeStart, rangeEnd);
  }
}
