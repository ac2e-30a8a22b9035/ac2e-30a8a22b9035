import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const UserContext = createParamDecorator(
  (data, context: ExecutionContext) => {
    return context.switchToHttp().getRequest().headers['userId'];
  },
);
