export class Config {

  public static getMongoDBConnectionUrl(): string {
    return 'mongodb://localhost:27017/flip';
  }

  public static getRabbitMQConnectionUrl(): string {
    return 'amqp://rabbitmq:rabbitmq@localhost:5672/';
  }

  public static getFeedAckQueueName() {
    return 'feed-ack';
  }

  public static getFeedFanoutQueueName() {
    return 'feed-fanout';
  }
}
