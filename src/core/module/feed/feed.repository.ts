import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Feed } from './feed.interface';
import { FeedState } from './feed.state';
import { Post } from '../post/post.interface';
import { ObjectId } from 'mongodb';

export class FeedRepository {

  constructor(@InjectModel('Feed') private readonly feedModel: Model<Feed>) {
  }

  public async createDefaultsAndSetState(userId: string, state: FeedState) {
    const feedModel = await this.feedModel.create({
      userId,
      optLock: 0,
      state,
      posts: [],
      rangeStart: null,
      rangeEnd: null,
    });
    return await feedModel.save();
  }

  public async getUserFeed(userId: string, postsLimit: number): Promise<Feed> {
    const [feed] = await this.feedModel.aggregate([{ $match: { userId } }, {
      $project: {
        userId: 1,
        optLock: 1,
        state: 1,
        rangeStart: 1,
        rangeEnd: 1,
        totalPosts: { $size: '$posts' },
        posts: { $slice: ['$posts', postsLimit] },
      },
    },
    ]).exec();
    return feed;
  }

  public setState(userId: any, optLock: number, state: FeedState) {
    return this.feedModel.updateOne({ userId, optLock }, {
      state,
    }, { $inc: { optLock: 1 } }).exec();
  }

  public addPostsAndSetState(userId: any, optLock: number, posts: Post[], state: FeedState) {
    const rangeStart = posts.length ? posts[0]._id : null;
    const rangeEnd = posts.length ? posts[posts.length - 1]._id : null;
    return this.feedModel.updateOne({ userId, optLock }, {
      state,
      rangeStart,
      rangeEnd,
      $push: { posts: { $each: posts } },
    }, { $inc: { optLock: 1 } }).exec();
  }

  public removePosts(userId: any, postIds: string[]) {
    return this.feedModel.updateOne({ userId }, {
      $pull: {
        posts: {
          _id: { $in: postIds.map(p => new ObjectId(p)) },
        } as any,
      },
    }).exec();
  }

}
