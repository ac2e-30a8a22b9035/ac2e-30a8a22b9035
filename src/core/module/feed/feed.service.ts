import { Injectable, NotImplementedException } from '@nestjs/common';
import { FeedRepository } from './feed.repository';
import { FeedFanoutEventProducerService } from '../feed-fanout-event-producer/feed-fanout-event-producer.service';
import { Feed } from './feed.interface';
import { Post } from '../post/post.interface';
import { FeedState } from './feed.state';
import { FeedAckEventProducerService } from '../feed-ack-event-producer/feed-ack-event-producer.service';
import * as BluebirdPromise from 'bluebird';

@Injectable()
export class FeedService {

  private CONFIG_API_POSTS_LIMIT = 3;

  constructor(private readonly feedRepository: FeedRepository,
              private readonly feedFanoutEventProducerService: FeedFanoutEventProducerService,
              private readonly feedAckEventProducerService: FeedAckEventProducerService) {
  }

  public async next(userId: string): Promise<Post[]> {
    const posts = await this.nextFeedPosts(userId);
    await this.feedAckEventProducerService.sendAck(userId, posts);
    return posts;
  }

  private async nextFeedPosts(userId: string): Promise<Post[]> {
    const feed = await this.feedRepository.getUserFeed(userId, this.CONFIG_API_POSTS_LIMIT);
    const feedState = this.defineFeedVirtualState(feed);

    switch (feedState) {
      case FeedState.FANOUT_START:
        await this.feedFanoutEventProducerService.sendFanoutStart(userId);
        return this.getFeedPostsWithDelay(userId, 10, 5000);

      case FeedState.FANOUT_IN_PROGRESS:
        return this.getFeedPostsWithDelay(userId, 10, 5000);

      case FeedState.FANOUT_PUSH:
        await this.feedFanoutEventProducerService.sendFanoutPush(userId, feed.optLock, feed.rangeStart, feed.rangeEnd);
        return feed.posts;

      case FeedState.FANOUT_COMPLETE:
        return feed.posts;

      default:
        throw new NotImplementedException();
    }
  }

  private defineFeedVirtualState(feed: Feed): FeedState {
    if (!feed) {
      return FeedState.FANOUT_START;
    }
    if (feed.state === FeedState.FANOUT_COMPLETE && feed.totalPosts <= this.CONFIG_API_POSTS_LIMIT * 3) {
      return FeedState.FANOUT_PUSH;
    }
    return feed.state;
  }

  private async getFeedPostsWithDelay(userId, times, timeout) {
    let stopProcess;
    const results = await BluebirdPromise.mapSeries(new Array(times).fill(1), async () => {
      if (!stopProcess) {
        const posts = await this.getFeedPosts(userId);
        if (posts) {
          stopProcess = true;
          return posts;
        }
        await new Promise(resolve => setTimeout(resolve, timeout));
      }
    });
    return results.find(result => result);
  }

  private async getFeedPosts(userId) {
    const feed = await this.feedRepository.getUserFeed(userId, this.CONFIG_API_POSTS_LIMIT);
    if (!feed) {
      return null;
    }
    if (feed.state === FeedState.FANOUT_COMPLETE) {
      return feed.posts;
    }
    return null;
  }

}
