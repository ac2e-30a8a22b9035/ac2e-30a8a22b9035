import { Document } from 'mongoose';
import { Post } from '../post/post.interface';
import { FeedState } from './feed.state';

export interface Feed extends Document {
  userId: string;
  optLock: number;
  state: FeedState;
  posts: Post[];
  rangeStart: string;
  rangeEnd: string;
  totalPosts?: number;
}
