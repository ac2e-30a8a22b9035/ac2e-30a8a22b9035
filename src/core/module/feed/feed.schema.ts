import * as mongoose from 'mongoose';
import { PostSchema } from '../post/post.schema';

export const FeedSchema = new mongoose.Schema({
  userId: { type: String, required: true },
  state: { type: String, required: true },
  optLock: { type: Number, required: true },
  rangeStart: { type: String, required: false },
  rangeEnd: { type: String, required: false },
  posts: [{ type: PostSchema, required: true }],
});

FeedSchema.index({ userId: 1, unique: true });
