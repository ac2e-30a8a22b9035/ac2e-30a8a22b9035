import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FeedSchema } from './feed.schema';
import { FeedService } from './feed.service';
import { FeedRepository } from './feed.repository';
import { FeedFanoutEventProducerModule } from '../feed-fanout-event-producer/feed-fanout-event-producer.module';
import { FeedAckEventProducerModule } from '../feed-ack-event-producer/feed-ack-event-producer.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Feed', schema: FeedSchema }]),
    FeedFanoutEventProducerModule,
    FeedAckEventProducerModule
  ],
  controllers: [],
  providers: [FeedRepository, FeedService],
  exports: [FeedRepository, FeedService],
})
export class FeedModule {
}
