import { Module } from '@nestjs/common';
import { FeedModule } from '../feed/feed.module';
import { FeedAckEventProcessorService } from './feed-ack-event-processor.service';

@Module({
  imports: [
    FeedModule,
  ],
  controllers: [],
  providers: [FeedAckEventProcessorService],
  exports: [FeedAckEventProcessorService],
})
export class FeedAckEventProcessorModule {
}
