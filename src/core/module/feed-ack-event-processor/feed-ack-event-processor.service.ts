import { FeedRepository } from '../feed/feed.repository';
import { Injectable } from '@nestjs/common';

@Injectable()
export class FeedAckEventProcessorService {

  constructor(private readonly feedRepository: FeedRepository) {
  }

  public async onPostAck(userId, postIds: string[]) {
    await this.feedRepository.removePosts(userId, postIds);
  }
}
