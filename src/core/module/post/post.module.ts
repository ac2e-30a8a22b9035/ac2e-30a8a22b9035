import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PostSchema } from './post.schema';
import { PostService } from './post.service';
import { PostRepository } from './post.repository';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Post', schema: PostSchema }])],
  controllers: [],
  providers: [PostRepository, PostService],
  exports: [PostRepository, PostService],
})
export class PostModule {
}
