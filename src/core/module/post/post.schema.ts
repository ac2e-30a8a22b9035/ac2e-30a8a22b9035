import * as mongoose from 'mongoose';

export const PostSchema = new mongoose.Schema({
  url: { type: String, required: true },
});
