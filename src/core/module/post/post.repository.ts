import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Post } from './post.interface';
import { Injectable } from '@nestjs/common';

@Injectable()
export class PostRepository {

  constructor(@InjectModel('Post') private readonly postModel: Model<Post>) {
  }

  public async initializePosts(numberOfPosts) {
    await Promise.all(new Array(numberOfPosts)
      .fill(1)
      .map(() => this.save(),
      ));
  }

  public async save() {
    const postModel = await this.postModel.create({
      url: new Date().getTime().toString(),
    });
    return postModel.save();
  }

  public async getPage(rangeStart, rangeEnd, limit) {
    const conditions = rangeStart ? {
      _id: {
        $lt: rangeEnd,
        $gt: rangeStart,
      },
    } : undefined;

    return this.postModel.find(conditions).limit(limit).exec();
  }
}
