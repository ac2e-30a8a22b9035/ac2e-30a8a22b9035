import { Document } from 'mongoose';

export interface Post extends Document {
  url: string;
}
