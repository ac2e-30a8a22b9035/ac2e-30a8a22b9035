import { FeedRepository } from '../feed/feed.repository';
import { FeedState } from '../feed/feed.state';
import { PostRepository } from '../post/post.repository';
import { Injectable } from '@nestjs/common';

@Injectable()
export class FeedFanoutEventProcessorService {

  constructor(private readonly feedRepository: FeedRepository,
              private readonly postRepository: PostRepository) {
  }

  public async onFanoutStart(userId: string) {
    const feed = await this.feedRepository.createDefaultsAndSetState(userId, FeedState.FANOUT_IN_PROGRESS);
    await this.makeFanout(userId, feed.optLock);
  }

  public async onFanoutPush(userId: string, optLock: number, rangeStart, rangeEnd) {
    await this.feedRepository.setState(userId, optLock, FeedState.FANOUT_IN_PROGRESS);
    await this.makeFanout(userId, optLock, rangeStart, rangeEnd);
  }

  private async makeFanout(userId: string, optLock: number, rangeStart?, rangeEnd?) {
    const posts = await this.postRepository.getPage(rangeStart, rangeEnd, 30);
    await this.feedRepository.addPostsAndSetState(userId, optLock, posts, FeedState.FANOUT_COMPLETE);
  }
}
