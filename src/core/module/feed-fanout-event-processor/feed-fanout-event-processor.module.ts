import { Module } from '@nestjs/common';
import { FeedFanoutEventProcessorService } from './feed-fanout-event-processor.service';
import { PostModule } from '../post/post.module';
import { FeedModule } from '../feed/feed.module';

@Module({
  imports: [
    PostModule,
    FeedModule,
  ],
  controllers: [],
  providers: [FeedFanoutEventProcessorService],
  exports: [FeedFanoutEventProcessorService],
})
export class FeedFanoutEventProcessorModule {
}
