export interface FeedAckAckEvent {
  userId: string;
  postIds: string[]
}
