import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { FEED_ACK_TOKEN } from '../amqp/amqp.token';
import { Config } from '../../config/Config';
import { FeedAckEventProducerService } from './feed-ack-event-producer.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: FEED_ACK_TOKEN, transport: Transport.RMQ,
        options: {
          urls: [Config.getRabbitMQConnectionUrl()],
          queue: Config.getFeedAckQueueName(),
          queueOptions: {
            durable: true,
          },
        },
      },
    ]),
  ],
  controllers: [],
  providers: [FeedAckEventProducerService],
  exports: [FeedAckEventProducerService],
})

export class FeedAckEventProducerModule {
}

