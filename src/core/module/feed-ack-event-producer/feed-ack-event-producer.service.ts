import { Inject, Injectable } from '@nestjs/common';
import { FEED_ACK_TOKEN } from '../amqp/amqp.token';
import { ClientProxy } from '@nestjs/microservices';
import { Post } from '../post/post.interface';
import { FeedAckAckEvent } from './feed-ack-ack.event';
import { FeedAckEvent } from './feed-ack.event';

@Injectable()
export class FeedAckEventProducerService {

  constructor(@Inject(FEED_ACK_TOKEN) private readonly client: ClientProxy) {
  }

  public async sendAck(userId, posts: Post[]) {
    const ackEvent: FeedAckAckEvent = { userId, postIds: posts.map(p => p._id) };
    await this.client.emit<any>(FeedAckEvent.ACK, ackEvent);
  }
}
