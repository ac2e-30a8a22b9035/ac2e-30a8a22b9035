export interface FeedFanoutPushEvent {
  userId: string,
  optLock: number,
  rangeStart: string,
  rangeEnd: string
}
