import { Post } from '../post/post.interface';
import { Inject, Injectable } from '@nestjs/common';
import { FEED_FANOUT_TOKEN } from '../amqp/amqp.token';
import { ClientProxy } from '@nestjs/microservices';
import { FeedFanoutPushEvent } from './feed-fanout-push.event';
import { FeedFanoutStartEvent } from './feed-fanout-start.event';
import { FeedFanoutEvent } from './feed-fanout.event';

@Injectable()
export class FeedFanoutEventProducerService {

  constructor(@Inject(FEED_FANOUT_TOKEN) private readonly client: ClientProxy) {
  }

  public async sendFanoutStart(userId: string) {
    const fanoutStartEvent: FeedFanoutStartEvent = { userId };
    await this.client.emit<any>(FeedFanoutEvent.START, fanoutStartEvent);
  }

  public async sendFanoutPush(userId: string, optLock: number, rangeStart, rangeEnd) {
    const fanoutPushEvent: FeedFanoutPushEvent = { userId, optLock, rangeStart, rangeEnd };
    await this.client.emit<any>(FeedFanoutEvent.PUSH, fanoutPushEvent);
  }
}
