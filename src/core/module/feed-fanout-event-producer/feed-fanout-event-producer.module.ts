import { FeedFanoutEventProducerService } from './feed-fanout-event-producer.service';
import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { FEED_ACK_TOKEN, FEED_FANOUT_TOKEN } from '../amqp/amqp.token';
import { Config } from '../../config/Config';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: FEED_FANOUT_TOKEN, transport: Transport.RMQ,
        options: {
          urls: [Config.getRabbitMQConnectionUrl()],
          queue: Config.getFeedFanoutQueueName(),
          queueOptions: {
            durable: true,
          },
        },
      },
    ]),
  ],
  controllers: [],
  providers: [FeedFanoutEventProducerService],
  exports: [FeedFanoutEventProducerService],
})
export class FeedFanoutEventProducerModule {
}
