## Installation

```bash
$ npm install
```

## Running the docker containers

```bash
$ cd docker
$ docker compose up -d
```

## Running the app

```bash
$ npm run run:feed-api
$ npm run run:feed-fanout
$ npm run run:feed-ack
```

## Swagger docs

```bash
http://localhost:3000/api
```
